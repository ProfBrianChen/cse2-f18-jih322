// jiaqi HAN, CSE 002
import java.util.Scanner;

public class WordTools {

	public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    String userinput = "";
    String carrier = "";
    String userinput1 = "";
    String carrier1 = "";
    int carrier2 = 0;
    String carrier3 = "";
    String carrier4 = "";
    userinput = sampleText(carrier);
    System.out.println("You entered : "+userinput);
    userinput1 = printMenu(carrier1);
    if(userinput1.equals("c")){
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(userinput));
    }// COMPUTATION that when user input c to ask the number of non-whitespace characters
    else if (userinput1.equals("w")){
      System.out.println("Number of words: " + getNumOfWords(userinput));// computation that when user input w to ask the number of words
    } else if(userinput1.equals("f")){
      System.out.println("Enter a word or phrase to be found: ");
      carrier3 =  myScanner.nextLine();
      carrier4 = carrier3;
      System.out.println("'"+ carrier3+ "'" + "instances: " + findText(userinput,carrier3));// computation that when suer input f to ask the number of repeatation of a phrase
    }else if(userinput1.equals("r")){
      System.out.println("edited text: " + replaceExclamation(userinput));// computation that when user input r to change ! into .
    }else if(userinput1.equals("s")){
      System.out.println("edited text: " + shortenSpace(userinput));// computation that when user input s to shaorten sapce.
    }
  }
  
  public static String sampleText( String a){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a sample text: ");
    a = myScanner.nextLine();// ask input 
    return a;
  }
  
  public static String printMenu( String b){
    String c = "";
    Scanner myScanner = new Scanner(System.in);
    do{// print menu
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println(" ");
    System.out.println("Choose an option:");
    c = myScanner.nextLine();
      if(c=="q"){
       break;// quit program
     }
    }while(c.equals("a") || c.equals("b") ||c.equals("d") ||c.equals("e") ||c.equals("g") ||c.equals("h") ||c.equals("i") ||c.equals("g") ||c.equals("k") ||c.equals("l") ||c.equals("m") ||c.equals("n") ||c.equals("o") ||c.equals("p") ||c.equals("t") ||c.equals("u") ||c.equals("v") ||c.equals("x") ||c.equals("y") ||c.equals("z"));
    return c;
  }
  
  public static int getNumOfNonWSCharacters(String f){
    int count = 0;
    for (int i=0; i<f.length(); i++) {
			if(('a' <= f.charAt(i))&&('z' >= f.charAt(i)) 
				||('A' <= f.charAt(i))&&('Z' >= f.charAt(i))) {
				count++;
			}
  }return count;
}
  
 public static int getNumOfWords(String f){
    int count = 0;
    int count1=0;
    for (int i=0; i<f.length(); i++) {
      if(' ' == f.charAt(i)) { 
        if( ' ' == f.charAt(i+1)){
          continue;
        }
				count++;
      }
    }
    count1=count+1;
    return count1;
  }
                         
  public static int findText(String f, String g){
   int count = 0;
    int index = 0;
    while ((index = f.indexOf(g, index)) != -1) {
        index = index + g.length();
        count++;
    }
    return count;
}
  
  public static String replaceExclamation(String f){
    String g ="";
    g=f.replace('!','.');
    return g;
  }
  
  public static String shortenSpace(String f){
    String g="";
      g=f.replaceAll(" {2,}", " ");
    return g;
  }
                         }
                         
                   
                  