import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    int students[] = new int[15];
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    for (int i = 0; i < 15; i++) {
      //test the type of input
      System.out.println("enter here: ");
      while (!myScanner.hasNextInt()) {
        System.out.println("Error");
        System.out.println("Please enter an integer: ");
        myScanner.next();
        continue;
      }
      students[i] = myScanner.nextInt();// test the range of input
      while (students[i] > 100 || students[i] < 0) {
        System.out.println("Error");
        System.out.println("Please enter an integer between 0 to 100: ");
        students[i] = myScanner.nextInt();
        continue;
      }
      if (i >= 1) {// test wheather the later is greater than last one
        while (students[i] < students[i - 1]) {
          System.out.println("Error");
          System.out.println("Please enter an integer greater or equal to the last one: ");
          students[i] = myScanner.nextInt();
          continue;}
      }
    }
    
  for(int i = 0; i<15; i++){
    System.out.print(students[i]);
  }
     System.out.println("please enter the grade that you want to find:");
  int grade = myScanner.nextInt();
   System.out.println(search(students, grade));
   System.out.println("Scrambled");
   scrambled(students);
   printArray(students);
   System.out.println("please enter the grade that you want to find:");
  int grade2 = myScanner.nextInt();
   System.out.println(search1(students, grade2));
  } 
  
  public static String search1(int[] arr, int key) {// linear search
    int count = 0;
    	for(int i=0;i<arr.length;i++){
        count++;
	    if(key==arr[i]){ 
        String a = key + " was found in the list with " + count + " iterations";
        return a;
      }
	} 
    String b = key + " was not found in the list with" + count +" iterations";
	  return b;
  }
  
  public static int[] printArray(int[] array){// get output
  for (int i=0; i<15; i++){ 
  System.out.print(array[i]+" "); // show the cards
  }
  System.out.println("");
  return array;
}
  
  public static int[] scrambled(int[] nums) {
    Random rnd = new Random();
    for (int i = 15 - 1; i > 0; i--) {
        int j = rnd.nextInt(i + 1);// swap the position
        int t = nums[i];
        nums[i] = nums[j];
        nums[j] = t;
    }
  return nums;
}
  
  public static String search(int[] arr, int key) {// binary search
       int start = 0;
       int end = arr.length - 1;
       int count = 0;      
       while (start <= end) {
         count++;
           int middle = (start + end) / 2;
           if (key < arr[middle]) {
               end = middle - 1;
           } else if (key > arr[middle]) {
               start = middle + 1;
           } else {
              String a = key + " was found in the list with " + count + " iterations";
               return a;
           }
       }  String b = key + " was not found in the list with" + count +" iterations";
    return b;
}
}