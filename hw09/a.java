import java.lang.Math;
import java.util.Scanner;
public class a{
  public static int[] randomInput(int[] array){
    for(int i=0; i<10; i++){
      array[i] = (int)(Math.random()*9);
    }
    return array;
  }
  
  public static int[] delete(int[] list, int pos){
    
    int[] newArray = new int[list.length - 1];
    int index = pos;
        for (int i = 0; i < list.length; i++) {
            if (i < index) {
                newArray[i] = list[i];
            } else if (i > index) {
                newArray[i - 1] = list[i];
            }
        }
    return newArray;
  }
 
  public static int[] remove(int[] list, int target){
    int count = 0;
		for(int i = 0 ; i<list.length ; i++){
			if(list[i]==target){
				count++;
			}
		}
 
		int[] newArr = new int[list.length-count];
			
		int index=0; 
		for(int i = 0; i<list.length ; i++){
			if(list[i]!=target){
				newArr[index] = list[i];
				index++;
			}
		}
		return newArr;
	}

  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index=0;
int target=0;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(num);
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
  

 
  	System.out.print("Enter the index ");
     index = scan.nextInt();
        
        while (index > 9|| index < 0){
       System.out.println("wrong input, please enter integer between 0-9");
        index = scan.nextInt();
     }

  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);

 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));}
  
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
