import java.util.Scanner;

public class Loop{
    // main method required for every Java program
   	public static void main(String[] args) {
      
      Scanner myScanner = new Scanner( System.in );
      
    System.out.println("please enter the course number: ");
      while( !myScanner.hasNextInt()){
        System.out.println("wrong input, please enter integer");
        myScanner.next();
      }
      
      int courseNum = myScanner.nextInt();
      
      System.out.println("please enter the department name: ");
       while( !myScanner.hasNext()){
        System.out.println("wrong input, please enter string");
        myScanner.next();
      }
      
      String departmentName = myScanner.next();
      
      System.out.println("please enter the number of times meets in a week: ");
       while( !myScanner.hasNextInt()){
        System.out.println("wrong input, please enter integer");
        myScanner.next();
      }
      
      int meetsNum = myScanner.nextInt();
      
      System.out.println("please enter the time the class starts : ");
       while( !myScanner.hasNext()){
        System.out.println("wrong input, please enter string");
        myScanner.next();
      }
      
      String classTime = myScanner.next();
      
      System.out.println("please enter the instructor name: ");
       while( !myScanner.hasNext()){
        System.out.println("wrong input, please enter string");
        myScanner.next();
      }
      
      String instructorName = myScanner.next();
      
      System.out.println("please enter the student number: ");
       while( !myScanner.hasNextInt()){
        System.out.println("wrong input, please enter integer");
        myScanner.next();
      }
      
      int studentNum = myScanner.nextInt();
      
    }
}