//CSE 002 JIAQI HAN HW03 CONVERT
// TO successfully run the scaanner class
import java.util.Scanner;

public class Convert{
  public static void main(String[] args){
    
    //declare an scanner object
    Scanner myScanner = new Scanner( System.in );
    
    System.out.print("Enter the affected area in acres: ");
    
    // allow the user to input
    double acreArea = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area:");
    
    double rainFall = myScanner.nextDouble();
    
    // compute the gallonsRain
    double gallonsRain = acreArea * rainFall * 27154;
    
    // convert to cubic
    double cubicRain = gallonsRain * 9.08169 * Math.pow(10,-13);
    
    // print the output
    System.out.print(cubicRain + "cubic miles");
  }
}