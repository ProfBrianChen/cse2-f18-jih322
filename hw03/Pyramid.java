//CSE 002 JIAQI HAN HW03 Pyramid
// TO successfully run the scaanner class
import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args){
    
    //declare an scanner object
    Scanner myScanner = new Scanner( System.in );
    
    System.out.print("Enter the square side of the pyramid is: ");
    
    // allow the user to input
    double pyramidLength = myScanner.nextDouble();
    
    System.out.print("Enter the height of the pyramid is: ");
    
    double pyramidHeight = myScanner.nextDouble();
     
    // convert to cubic
    double pyramidVolume = (pyramidLength * pyramidLength * pyramidHeight) / 3;
    
    // print the output
    System.out.print("the volume inside the pyramid is:" + pyramidVolume);
  }
}
// end