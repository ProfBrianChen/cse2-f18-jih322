// card generator CSE02 jiaqi han
import java.lang.Math;
public class CardGenerator{
  public static void main(String[] args) {
    int number = (int)(Math.random()*52)+1;
      System.out.println("the generate number is:" + number);
    
    String suitname = " ";
    String identity = " ";
    
    if (number >= 1 && number <= 13){
      suitname = "Dimond";
    } else if (number >= 14 && number <= 26){
      suitname = "Club";
    } else if (number >= 27 && number <= 39){
      suitname = "Heart";
    } else if (number >= 40 && number <= 52){
      suitname = "Spade";
    } else {
      suitname = "invalid";
    }
    
    System.out.println("the suit is " + suitname);
    
    int modolus;
    modolus = number % 13;
    System.out.println("the modolus is:" + modolus);
    switch( modolus ) {
      case 0 : identity = "king";
        break;
      case 1 : identity = "1";
        break;
      case 2 : identity = "2";
        break;
      case 3 : identity = "3";
        break;
      case 4 : identity = "4";
        break;
      case 5 : identity = "5";
        break;
      case 6 : identity = "6";
        break;
      case 7 : identity = "7";
        break;
      case 8 : identity = "8";
        break;
      case 9 : identity = "9";
        break;
      case 10 : identity = "10";
        break;
      case 11 : identity = "Jack";
        break;
      case 12 : identity = "Queen";
        break;
      default:
        System.out.println("the card may not be in the suit");
    }
    System.out.println("the identity is " + identity);
    System.out.println("you picked the " + identity + " of " + suitname);
      
        
  }
}