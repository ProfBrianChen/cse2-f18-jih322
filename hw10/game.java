import java.util.Scanner;

public class game {
    public static void main(String[] args) {
        char[][] array = {		//The board
		{'1','2','3'},		
		{'4','5','6'},		
		{'7','8','9'}	
			};
        Scanner input = new Scanner(System.in);
        int x=0;
        int y=0;
        int temp=0;
        output(array);
        for(int i=0;i<9;i++) {
             System.out.println(" enter the position number that wish to mark.");// ask input
             
          while( !input.hasNextInt()){// determine the type of input
                 System.out.println("wrong input, please enter integer");
                 input.next();
      }
              temp=input.nextInt();
      
             while(temp<1||temp>9){// determine the range of the input
                System.out.println("out of range! enter again");
                temp=input.nextInt();
                }
              if(temp==1){// switch the input to the coordinate, whcih help to locate
                x=1;
                y=1;
              }else if(temp==2){
                x=1;
                y=2;
              }else if(temp==3){
                x=1;
                y=3;
              }else if(temp==4){
                x=2;
                y=1;
              }else if(temp==5){
                x=2;
                y=2;
              }else if(temp==6){
                x=2;
                y=3;
              }else if(temp==7){
                x=3;
                y=1;
              }else if(temp==8){
                x=3;
                y=2;
              }else if(temp==9){
                x=3;
                y=3;
              }
            x--;y--;
            if(array[x][y]=='x') {//  determine the repetition number location
                do {System.out.println("repeat! enter again!");
                     temp=input.nextInt();
                     if(temp==1){
                x=1;
                y=1;
              }else if(temp==2){
                x=1;
                y=2;
              }else if(temp==3){
                x=1;
                y=3;
              }else if(temp==4){
                x=2;
                y=1;
              }else if(temp==5){
                x=2;
                y=2;
              }else if(temp==6){
                x=2;
                y=3;
              }else if(temp==7){
                x=3;
                y=1;
              }else if(temp==8){
                x=3;
                y=2;
              }else if(temp==9){
                x=3;
                y=3;
              }
                x--;y--;
                }while(temp<1||temp>9||array[x][y]=='x');
             
            } else if(array[x][y]=='o'){//  determine the repetition number location
                do { System.out.println("repeat! enter again!");
                     temp=input.nextInt(); if(temp==1){
                x=1;
                y=1;
              }else if(temp==2){
                x=1;
                y=2;
              }else if(temp==3){
                x=1;
                y=3;
              }else if(temp==4){
                x=2;
                y=1;
              }else if(temp==5){
                x=2;
                y=2;
              }else if(temp==6){
                x=2;
                y=3;
              }else if(temp==7){
                x=3;
                y=1;
              }else if(temp==8){
                x=3;
                y=2;
              }else if(temp==9){
                x=3;
                y=3;
              }
                x--;y--;
                }while(temp<1||temp>9||array[x][y]=='o');
             
            }
            if(i%2==0){// determine what char is entering
                array[x][y]='x';
            }else{
                array[x][y]='o';
            }
            output(array);
            switch(judge(array)) {
            case 1:System.out.println("x representative win!");i=9;break;
            case 2:System.out.println("o representative win!");i=9;break;
            }
            if(i!=9) {
                if(i%2==0)// determine who is in the next step
                    System.out.print("Now is the 'o' player ");
                else
                    System.out.print("Now is the 'x' player ");
            }
        }
    }
  
    static void output(char[][] array) {// method to locate 
        System.out.println("-------------");
        System.out.printf("| %c | %c | %c |\n", array[0][0],array[0][1],array[0][2]);
        System.out.println("-------------");
        System.out.printf("| %c | %c | %c |\n", array[1][0],array[1][1],array[1][2]);
        System.out.println("-------------");
        System.out.printf("| %c | %c | %c |\n", array[2][0],array[2][1],array[2][2]);
        System.out.println("-------------");
    }
  
    static int judge(char[][] array) {//method to judge wheather the player win!
        for(int i=0;i<3;i++) {
            if(array[i][0]==array[i][1]&&array[i][1]==array[i][2]&&array[i][2]=='x')
                return 1;
            if(array[0][i]==array[1][i]&&array[1][i]==array[2][i]&&array[2][i]=='x')
                return 1;
        }
        if(array[0][0]==array[1][1]&&array[1][1]==array[2][2]&&array[2][2]=='x')
            return 1;
        if(array[0][2]==array[1][1]&&array[1][1]==array[2][0]&&array[2][0]=='x')
            return 1;
        for(int i=0;i<3;i++) {
            if(array[i][0]==array[i][1]&&array[i][1]==array[i][2]&&array[i][2]=='o')
                return 2;
            if(array[0][i]==array[1][i]&&array[1][i]==array[2][i]&&array[2][i]=='o')
                return 2;
        }
        if(array[0][0]==array[1][1]&&array[1][1]==array[2][2]&&array[2][2]=='o')
            return 2;
        if(array[0][2]==array[1][1]&&array[1][1]==array[2][0]&&array[2][0]=='o')
            return 2;
        return 0;
    }

}